import './bootstrap';

import Swal from "sweetalert2";
window.Swal = Swal;

// BASE
import '../assets/js/bootstrap.bundle.min.js'
import '../assets/js/jquery.toast.js'
import '../assets/js/jquery.validate.min.js'
import '../assets/js/feather.min.js'
import '../assets/js/simplebar.min.js'

// DATATABLES
import '../assets/plugins/datatables/jquery.dataTables.min.js'
import '../assets/plugins/datatables/dataTables.bootstrap5.min.js'
import '../assets/plugins/datatables/dataTables.buttons.min.js'
import '../assets/plugins/datatables/buttons.html5.min.js'
import '../assets/plugins/datatables/dataTables.responsive.min.js'

// SELECT2
import '../assets/plugins/select2/select2.min.js'

// CHART
import '../assets/plugins/apex-charts/apexcharts.min.js'

// PICKER
import '../assets/plugins/daterangepicker/daterangepicker.js'

// FORM
import '../assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js'
import '../assets/plugins/jquery-steps/jquery.steps.min.js'
import '../assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js'
import '../assets/plugins/tinymce/tinymce.min.js'
import '../assets/js/jquery.tagselect.js'
import '../assets/plugins/croppie/croppie.min.js'

// GLOBAL APP
import '../assets/js/app.js'
import '../assets/js/global'
