import { globalTable, filterTable } from "../../global";

$(function () {

    const table_name = '#activity-table'
    const endpoint = 'activity/list'
    const filter_data = (value) => {
        value.log_name   = $('#log-name').val()
        value.staff_name = $('#staff-name').val()
        value.email      = $('#email').val()
        value.role       = $('#role').val()
        value.event      = $('#event').val()
        value.start_date = $('#start_date').val()
        value.end_date   = $('#end_date').val()
        value.search     = $('input[type="search"]').val()
    }
    const columns = [
        {data: 'id', name: 'id', width: 30},
        {data: 'log_name', name: 'log_name'},
        {data: 'event', name: 'event', class: 'text-center'},
        {data: 'causer.name', name: 'causer.name'},
        {data: 'causer.email', name: 'causer.email'},
        {data: 'causer.role.name', name: 'causer.role.name'},
        {data: 'created_at', name: 'created_at'},
        {data: 'action', name: 'action', className: 'text-center dt-control', orderable: false, searchable: false},
    ]

    globalTable(table_name, endpoint, filter_data, columns);
});

$('#search-form').on('submit', function(e) {
    filterTable()
    e.preventDefault();
});
