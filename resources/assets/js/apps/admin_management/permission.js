import { actionPost } from "../../global";
window.checkedCrud = checkedCrud
window.checkedGrup = checkedGrup
window.actionPermission = actionPermission

$(function(){
    let role_id = $('#role_id').val();
    let endpoint  = window.location.origin + '/roles/permission/get/'+ role_id;

    $.get(endpoint, function( data ) {
        let levelToMenu = data;

        for (let x = 0; x < levelToMenu.length; x++) {

            let menu_x = $('#menu_id_' + levelToMenu[x].menu_id);
            menu_x != null ? menu_x.prop('checked', true) : menu_x.prop('checked', false)

            let create_role_x = $('input#create_' + levelToMenu[x].menu_id);
            levelToMenu[x].create == 1 ? create_role_x.prop('checked', true) : create_role_x.prop('checked', false);

            let read_role_x = $('input#read_' + levelToMenu[x].menu_id);
            levelToMenu[x].read == 1 ? read_role_x.prop('checked', true) : read_role_x.prop('checked', false);

            let update_role_x = $('input#update_' + levelToMenu[x].menu_id);
            levelToMenu[x].update == 1 ? update_role_x.prop('checked', true) : update_role_x.prop('checked', false);

            let delete_role_x = $('input#delete_' + levelToMenu[x].menu_id);
            levelToMenu[x].delete == 1 ? delete_role_x.prop('checked', true) : delete_role_x.prop('checked', false)
        }
    });
});

function checkedGrup (val, id) {

    $('input#create_'+id).prop('checked', val.checked);
    $('input#read_'+id).prop('checked', val.checked);
    $('input#update_'+id).prop('checked', val.checked);
    $('input#delete_'+id).prop('checked', val.checked);
    actionPermission(id);
}

function checkedCrud (val, id) {

    actionPermission(id);

    if ($('#read_'+id).is(':checked') || $('#create_'+id).is(':checked') || $('#update_'+id).is(':checked') || $('#delete_'+id).is(':checked')) {
        $('#menu_id_'+id).prop('checked', 'true');
    } else {
        $('#menu_id_'+id).prop('checked', val.checked);
    }
}

function actionPermission (id) {
    const data = {
        menu_id : id,
        role_id: $('#role_id').val(),
        create : $('#create_' + id).is(':checked'),
        read : $('#read_' + id).is(':checked'),
        update : $('#update_' + id).is(':checked'),
        delete : $('#delete_' + id).is(':checked')
    }
    const endpoint = 'roles/permission';

    actionPost('POST', data, endpoint);
}
