import { globalTable, actionPost, alertDelete, filterTable } from "../../global";
window.openModalLevel = openModalLevel
window.deleteRoles = deleteRoles

const table_name = '#table-level'
const endpoint = 'roles/list'

$(function () {
    const filter_data = (value) => { value.search = $('input[type="search"]').val() }
    const columns = [
        {data: 'id', name: 'id', width: 30},
        {data: 'name', name: 'name'},
        {data: 'guard_name', name: 'guard_name'},
        {data: 'created_at', name: 'created_at'},
        {data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false},
    ]

    globalTable(table_name, endpoint, filter_data, columns);
});

function openModalLevel (id, name, type) {

    id == 'add' ? $('#modal_level_header').text('Tambah User Role') : $('#modal_level_header').text('Edit User Role');
    id == 'add' ? $('#user_level_name').val('') : $('#user_level_name').val(name);
    id == 'add' ? $('#user_level_id').val('') : $('#user_level_id').val(id);
    id == 'add' ? $('.modal-header').removeClass('bg-success').addClass('bg-primary') : $('.modal-header').removeClass('bg-primary').addClass('bg-success');
    id == 'add' ? $('#submit-roles').removeClass('btn-success').addClass('btn-primary') : $('#submit-roles').removeClass('btn-primary').addClass('btn-success');
    id == 'add' ? '' : $(`#tipe option[value=${type}]`).attr('selected', true);

    $('#modal_level').modal({backdrop: 'static', keyboard: false});
    $('#modal_level').modal('show').on('shown.bs.modal', function() {
        $('#user_level_name').trigger('focus');
    });
}

$(".form-level").validate({
    rules: {
        name : {
            required: true
        },
        guard : {
            required: true
        },
    },
    submitHandler: function() {
        const submit    = $('#submit-roles');
        const cancel    = $('#close-modal');
        const modal     = $('#modal_level');
        const form_data = $('form.form-level').serialize();
        const endpoint  = 'roles';
        const type      = $('#user_level_id').val() == '' ? 'POST' : 'PATCH';

        actionPost(type, form_data, endpoint, submit, cancel, modal, table_name);
    }
});

async function deleteRoles (id) {

    const endpoint  = window.location.origin + '/roles';
    const result    = await alertDelete(id, endpoint);

    if(result.code == 200) {
        $.toast({heading: 'Berhasil', text: result.message, icon: 'success', position: 'top-right'});
        setTimeout(() => {
            filterTable()
        }, 1000);
    }
}
