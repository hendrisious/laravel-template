import { globalTable, actionPost, alertDelete, filterTable } from "../../global";

window.editAdmin = editAdmin
window.changeStatus = changeStatus
window.deleteAdmin = deleteAdmin

const table_name = '#admin-table'
const endpoint = 'admin/list'

$(function () {
    const filter_data = (value) => {
        value.name      = $('#name').val()
        value.email     = $('#email').val()
        value.handphone = $('#handphone').val()
        value.role      = $('#role').val()
        value.status    = $('#status').val()
        value.search    = $('input[type="search"]').val()
    }
    const columns = [
        {data: 'id', name: 'id', width: 30},
        {data: 'name', name: 'name'},
        {data: 'email', name: 'email'},
        {data: 'handphone', name: 'handphone'},
        {data: 'role', name: 'role'},
        {data: 'verified', name: 'verified', className: 'text-center'},
        {data: 'actived', name: 'actived'},
        {data: 'created_at', name: 'created_at'},
        {data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false},
    ]

    globalTable(table_name, endpoint, filter_data, columns);
});

$('#search-form').on('submit', function(e) {
    filterTable()
    e.preventDefault();
});

$('#add').on('click', function(e) {
    $('#add-admin-modal').modal({backdrop: 'static', keyboard: false});
    $('#add-admin-modal').modal('show').on('shown.bs.modal', function() {
        $('#name-input').trigger('focus');
    });
})

function editAdmin(id, name, email, role_id, active) {
    $('#edit-admin-modal').modal({backdrop: 'static', keyboard: false})
    $('#edit-admin-modal').modal('show')
    $('#name-text').text(name.toUpperCase())
    $('#email-text').text(email.toUpperCase())
    $('#role-input-edit option[value="'+role_id+'"]').prop('selected', true)

    $('#submit-edit-admin').on('click', function(e) {
        const submit    = $('#submit-edit-admin');
        const cancel    = $('.close');
        const modal     = $('#edit-admin-modal');
        const form_data = {
            id: id,
            role_id: $('#role-input-edit option:selected').val(),
            active: active
        };
        const endpoint  = 'admin';

        actionPost('PATCH', form_data, endpoint, submit, cancel, modal, table_name);
    });
}

function changeStatus(id, role_id, value){
    const form_data = {
        id: id,
        role_id: role_id,
        active: value.checked == true ? 1 : 0
    };
    const endpoint = 'admin';

    actionPost('PATCH', form_data, endpoint);
}

$("#form-register").validate({
    rules: {
        name: {
            required: true
        },
        email: {
            required: true
        },
        password: {
            required: true
        },
        password_confirmation: {
            required: true
        },
    },
    submitHandler: function(form) {

        const submit    = $('#submit-register');
        const cancel    = $('#close-modal');
        const modal     = $('#add-admin-modal');
        const form_data = $('form#form-register').serialize();
        const endpoint  = 'admin';

        actionPost('POST', form_data, endpoint, submit, cancel, modal, table_name);

    }
});

async function deleteAdmin(id) {

    const endpoint  = window.location.origin + '/admin';
    const result    = await alertDelete(id, endpoint);

    if(result.code == 200) {
        $.toast({heading: 'Berhasil', text: result.message, icon: 'success', position: 'top-right'});
        setTimeout(() => {
            filterTable()
        }, 1000);
    }
}
