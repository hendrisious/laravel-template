jQuery.validator.setDefaults({
    errorElement: 'span',
    errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
    },
    highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});

$(".form-signin").validate({
    rules: {
        email: {
            required: true
        },
        password: {
            required: true
        },
    },
    messages: {
        email: {
            required: "Masukan email anda"
        },
        password: {
            required: "Masukan password anda",
        },
    },
    submitHandler: function(form) {
        submitLogin();
    }
});

function submitLogin() {
    $('#submit-login').html("<div class='spinner-border spinner-border-sm me-2'></div> Tunggu Sebentar").attr('disabled', 'disabled');

    $.ajax({
        type: "POST",
        url:  window.location.origin + '/login',
        cache: false,
        data: $('form.form-signin').serialize(),
        success: function(response) {
            if (response.code == 200) {
                $.toast({heading: 'Berhasil', text: 'Halaman akan dialihkan', icon: 'success', position: 'top-center'})

                setTimeout(() => {
                    window.location = response.redirect;
                }, 1000);
            }
        },
        error: function(response) {
            let errors = response.responseJSON.errors.message
            errors.forEach(value => {
                $.toast({heading: 'Gagal', text: value, icon: 'error', position: 'top-right'})
            });
            $('#submit-login').html("Masuk <i class='fas fa-sign-in-alt ms-1'></i>").attr('disabled', false);
        }
    });
}
