$(document).ready(function() {

    $.get(base_url + 'dashboard/data_chart', function(response) {
        let data_response = $.parseJSON(response);
        let data_chart  = data_response.charts;
        let chart       = [];

        data_chart.forEach(function(value) {
            chart.push({
                name: value.type,
                data: value.total
            })
        })

        show_chart(chart, data_response.month);
    })

});


function show_chart(charts, month) {
    let options = {
        series: charts,
        chart: {
            type: 'bar',
            height: 400
        },
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '55%',
                endingShape: 'rounded'
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        xaxis: {
            categories: month,
        },
        yaxis: {
            title: {
                text: 'Total Transaksi'
            }
        },
        fill: {
            opacity: 1
        },
        tooltip: {
            y: {
                formatter: function(val) {
                    return val + " Transaksi"
                }
            }
        }
    };
    
    let chart = new ApexCharts(document.querySelector("#chart"), options);
    chart.render();
}
