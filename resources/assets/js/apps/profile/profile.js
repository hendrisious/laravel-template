import { event_submit } from "../../global";
window.submitProfile = submitProfile
window.error_submit = error_submit

$(".form-profile").validate({
    rules: {
        email: {
            required: true
        },
        name: {
            required: true
        },
    },
    messages: {
        email: {
            required: "Email wajib di isi"
        },
        name: {
            required: "Nama wajib di isi",
        },
    },
    submitHandler: function(form) {
        submitProfile();
    }
});

function submitProfile () {
    $('#submit-profile').html("<div class='spinner-border spinner-border-sm me-2'></div> Tunggu Sebentar").attr('disabled', 'disabled');

    $.ajax({
        type: "patch",
        enctype: "multipart/form-data",
        url:  window.location.origin + '/profile',
        cache: false,
        data: $('form.form-profile').serialize(),
        success: function(response) {
            if (response.code == 200) {
                $.toast({heading: 'Berhasil', text: response.message, icon: 'success', position: 'top-right'})
                setTimeout(() => {
                    $('#submit-profile').html("Simpan Profil").attr('disabled', false);
                }, 1000);
            }
        },
        error: function(response) {
            let errors = response.responseJSON.errors.message
            errors.forEach(value => {
                $.toast({heading: 'Gagal', text: value, icon: 'error', position: 'top-right'})
            });
            $('#submit-profile').html("Simpan Profil").attr('disabled', false);
        }
    });
}

$(function() {
    let croppie = null;
    let el      = document.getElementById('resizer');

    $.base64ImageToBlob = function(str) {
        // extract content type and base64 payload from original string
        let pos  = str.indexOf(';base64,');
        let type = str.substring(5, pos);
        let b64  = str.substr(pos + 8);

        // decode base64
        let imageContent = atob(b64);

        // create an ArrayBuffer and a view (as unsigned 8-bit)
        let buffer = new ArrayBuffer(imageContent.length);
        let view   = new Uint8Array(buffer);

        // fill the view, using the decoded base64
        for (let n = 0; n < imageContent.length; n++) {
          view[n] = imageContent.charCodeAt(n);
        }

        // convert ArrayBuffer to Blob
        let blob = new Blob([buffer], { type: type });

        return blob;
    }

    $.getImage = function(input, croppie) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function(e) {
                croppie.bind({
                    url: e.target.result,
                });
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#file-upload").on("change", function(event) {
        $('#modal-upload').modal({backdrop: 'static', keyboard: false});
        $("#modal-upload").modal('show');
        // Initailize croppie instance and assign it to global variable
        croppie = new Croppie(el, {
                viewport: {
                    width: 200,
                    height: 200,
                    type: 'square'
                },
                boundary: {
                    width: 250,
                    height: 250
                },
                enableOrientation: true
            });
        $.getImage(event.target, croppie);
    });

    $("#upload").on("click", function() {
        croppie.result('base64').then(function(base64) {

            const submit = $('#upload');
            const cancel = $('#close-modal');

            event_submit(submit, cancel);

            let formData = new FormData();
            formData.append("profile_picture", $.base64ImageToBlob(base64));

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "POST",
                url:  window.location.origin + '/profile',
                data: formData,
                processData: false,
                contentType: false,
                success: function (response) {
                    if (response.code == 200) {
                        $("#profile-pic").attr("src", base64);
                        $.toast({heading: 'Berhasil', text: response.message, icon: 'success', position: 'top-right'})
                    }
                },
                error: function (response) {
                    let errors = response.responseJSON.errors.profile_picture
                    errors.forEach(value => {
                        $.toast({heading: 'Gagal', text: value, icon: 'error', position: 'top-right'})
                    });
                    error_submit(submit, cancel);
                }
            });

            setTimeout(() => {
                $("#modal-upload").modal("hide");
                error_submit(submit, cancel);
            }, 1500);

        });
    });

    // To Rotate Image Left or Right
    $(".rotate").on("click", function() {
        croppie.rotate(parseInt($(this).data('deg')));
    });

    $('#modal-upload').on('hidden.bs.modal', function (e) {
        setTimeout(function() { croppie.destroy(); }, 100);
    });

    $('#cancel-upload').on('click', function() {
        $('#modal-upload').modal('hide');
    });
});

function error_submit(submit, cancel) {
    submit.html("Upload Sekarang").prop('disabled', false);
    cancel.prop('disabled', false);
}
