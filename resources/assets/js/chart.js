$(function() {
    $('#reportrange_span').daterangepicker({
        opens: 'center',
        ranges: {
            'Hari Ini': [moment(), moment()],
            'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
            '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
            'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
            'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, function(start, end, label) {
        $('#start_date').val(start.format('D MMM YYYY'));
        $('#end_date').val(end.format('D MMM YYYY'));
        $('#reportrange').val(start.format('D MMM YYYY') +' - '+ end.format('D MMM YYYY'))
    });
});

var options = {
    chart: { height: 300, type: "donut" },
    plotOptions: { pie: { donut: { size: "0%" } } },
    dataLabels: { enabled: true },
    stroke: { show: !0, width: 2, colors: ["transparent"] },
    series: [40, 50, 10],
    legend: { show: !0, position: "bottom", horizontalAlign: "center", verticalAlign: "middle", floating: !1, fontSize: "11px", offsetX: 0, offsetY: 5 },
    labels: ["Go Show", "Online", "Agent"],
    responsive: [{ breakpoint: 600, options: { plotOptions: { donut: { customScale: 0.2 } }, chart: { height: 300 }, legend: { show: !1 } } }],
    tooltip: {
        y: {
            formatter: function (e) {
                return e + " %";
            },
        },
    },
};
(chart = new ApexCharts(document.querySelector("#ana_device"), options)).render();

var options = {
    chart: { height: 300, type: "donut" },
    plotOptions: { pie: { donut: { size: "0%" } } },
    dataLabels: { enabled: true },
    stroke: { show: !0, width: 2, colors: ["transparent"] },
    series: [40, 50, 10],
    legend: { show: !0, position: "bottom", horizontalAlign: "center", verticalAlign: "middle", floating: !1, fontSize: "11px", offsetX: 0, offsetY: 5 },
    labels: ["Dewasa", "Anak", "Bayi"],
    responsive: [{ breakpoint: 600, options: { plotOptions: { donut: { customScale: 0.2 } }, chart: { height: 300 }, legend: { show: !1 } } }],
    tooltip: {
        y: {
            formatter: function (e) {
                return e + " %";
            },
        },
    },
};
(chart = new ApexCharts(document.querySelector("#ana_device2"), options)).render();

var options = {
    chart: { height: 300, type: "donut" },
    plotOptions: { pie: { donut: { size: "0%" } } },
    dataLabels: { enabled: true },
    stroke: { show: !0, width: 2, colors: ["transparent"] },
    series: [63, 37],
    legend: { show: !0, position: "bottom", horizontalAlign: "center", verticalAlign: "middle", floating: !1, fontSize: "11px", offsetX: 0, offsetY: 5 },
    labels: ["Laki-laki", "Perempuan"],
    responsive: [{ breakpoint: 600, options: { plotOptions: { donut: { customScale: 0.2 } }, chart: { height: 300 }, legend: { show: !1 } } }],
    tooltip: {
        y: {
            formatter: function (e) {
                return e + " %";
            },
        },
    },
};
(chart = new ApexCharts(document.querySelector("#ana_device3"), options)).render();

var options = {
    series: [{ name: "Sales per day", data: [10000000, 41000000, 35000000, 51000000, 49000000, 62000000, 69000000, 91000000, 48000000, 10000000, 41000000, 35000000, 51000000, 49000000, 62000000, 69000000, 91000000, 48000000,10000000, 41000000, 35000000, 51000000, 49000000, 62000000, 69000000, 91000000, 48000000, 10000000, 41000000, 35000000, 76000000] }],
    chart: { height: 200, type: "line", zoom: { enabled: !1 } },
    dataLabels: { enabled: !0, style: {colors: ['#fff']} },
    stroke: { curve: "straight", width: [3] },
    grid: { row: { colors: ["#f3f3f3", "transparent"], opacity: 0.5 } },
    xaxis: { categories: ["1", "2", "3", "4", "5", "6", "7", "8", "9","10", "11", "12", "13", "14", "15", "16", "17", "18","19", "20", "21", "22", "23", "24", "25", "26", "27","28", "29", "30", "31"] },
};
(chart = new ApexCharts(document.querySelector("#apex_line1"), options)).render();

var options = {
    series: [{ name: "Sales per month", data: [10, 41, 35, 51, 49, 62, 69, 91, 48, 69, 91, 48] }],
    chart: { height: 200, type: "bar", zoom: { enabled: !1 } },
    dataLabels: { enabled: !0, style: {colors: ['#000']} },
    stroke: { curve: "straight", width: [3] },
    grid: { row: { colors: ["#f3f3f3", "transparent"], opacity: 0.5 } },
    xaxis: { categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep","Okt","Nov","Dec"] },
};
(chart = new ApexCharts(document.querySelector("#apex_line2"), options)).render();

options = {
    chart: { height: 430, type: "bar", toolbar: { show: !1 } },
    plotOptions: { bar: { horizontal: !0 } },
    dataLabels: { enabled: !0 },
    series: [{ data: [400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380, 1400] }],
    stroke: { curve: "straight", width: [3] },
    yaxis: { axisBorder: { show: !0, color: "#bec7e0" }, axisTicks: { show: !0, color: "#bec7e0" } },
    xaxis: { categories: ["KM. MARINA EXPRESS 6", "KM. MARINA EXPRESS 3B", "KM. MARINA EXPRESS 3", "KM. EXPRESS BAHARI 9E", "KM. EXPRESS BAHARI 99B", "KM. EXPRESS BAHARI 99", "KM. EXPRESS BAHARI 88B", "KM. EXPRESS BAHAP 88", "KM. EXPRESS BELIBIS 8", "KM. EXPRESS BAHARI 7E", "KM. CANTIKA LESTARI 77"] },
    states: { hover: { filter: "none" } },
    grid: { borderColor: "#f1f3fa" },
};
(chart = new ApexCharts(document.querySelector("#apex_bar"), options)).render();

options = {
    chart: { height: 580, type: "bar", toolbar: { show: !1 } },
    plotOptions: { bar: { horizontal: !0 } },
    dataLabels: { enabled: !0 },
    series: [{ data: [400, 430, 448, 470, 540, 580, 690, 730, 870, 980, 1030, 1129, 1232, 1356, 1478, 1587, 1689, 1789, 1890] }],
    yaxis: { axisBorder: { show: !0, color: "#bec7e0" }, axisTicks: { show: !0, color: "#bec7e0" } },
    xaxis: { categories: ["BIAK", "JAYAPURA", "KASO", "KOLAKA KOTA", "LUWUK", "WAMESA", "MANOKWARI", "ROSWAR", "SERUI", "SIWA","SALAKAN","SORONG","SOUGWEPU","TOBAKU","RAJA AMPAT","WAROPEN","WASIOR","WINDESI","YENDE"] },
    states: { hover: { filter: "none" } },
    grid: { borderColor: "#f1f3fa" },
};
(chart = new ApexCharts(document.querySelector("#apex_bar2"), options)).render();

options = {
    chart: { height: 1070, type: "bar", toolbar: { show: !1 } },
    plotOptions: { bar: { horizontal: !0 } },
    dataLabels: { enabled: !0 },
    series: [{ data: [400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380,400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380,400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380,400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380,400, 430, 448, 470, 540, 580] }],
    yaxis: { axisBorder: { show: !0, color: "#bec7e0" }, axisTicks: { show: !0, color: "#bec7e0" } },
    xaxis: { categories: ["JAYAPURA - KASO", "SIWA - TOBAKU", "SIWA - KOLAKA KOTA", "SORONG - WAISAI (RAJA AMPAT)", "KASO - JAYAPURA", "TOBAKU - SIWA", "KOLAKA - SIWA", "WAISAI (RAJA AMPAT) - SORONG", "WASIOR (MANOKWARI) - WINDESI", "WINDESI - WAMESA","WAMESA - SOUGWEPU","SERUI - BIAK","BIAK - WAROPEN","SOUGWEPU - WAMESA","WAMESA - WINDESI","WINDESI - WASIOR (MANOKWARI)","WAROPEN - BIAK","BIAK - SERUI","WAROPEN - SERUI","SERUI - WAROPEN","LUWUK - SALAKAN","SALAKAN - LUWUK","MANOKWARI - SOUGWEPU","SOUGWEPU - MANOKWARI","MANOKWARI - WAMESA","WAMESA - MANOKWARI","MANOKWARI - WINDESI","WINDESI - MANOKWARI","MANOKWARI - WASIOR","WASIOR - MANOKWARI","MANOKWARI - ROSWAR","ROSWAR - MANOKWARI","MANOKWARI - YENDE","YENDE - MANOKWARI","SOUGWEPU - WINDESI","WINDESI - SOUGWEPU","SOUGWEPU - WASIOR","WASIOR - SOUGWEPU","WAMESA - WASIOR","WASIOR - WAMESA","ROSWAR - YENDE","YENDE - ROSWAR","ROSWAR - WASIOR","WASIOR - ROSWAR","YENDE - WASIOR","WASIOR - YENDE"] },
    states: { hover: { filter: "none" } },
    grid: { borderColor: "#f1f3fa" },
};
(chart = new ApexCharts(document.querySelector("#apex_bar3"), options)).render();
