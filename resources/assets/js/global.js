let table;
const base_url = window.location.origin + '/';
const token    = $("meta[name='csrf-token']").attr("content");

export function globalTable (table_name, endpoint, filter_data, columns) {
    table = $(table_name).DataTable({
        fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            $('td:eq(0)', nRow).html(`<center>${iDisplayIndexFull + 1}.</center>`);
        },
        processing: true,
        serverSide: true,
        language: {
            url: '//cdn.datatables.net/plug-ins/1.13.5/i18n/id.json',
        },
        ajax: {
          url: base_url + endpoint,
          data: filter_data
        },
        columns: columns
    });

    // SHOW DETAIL
    const detailRows = [];

    table.on('click', 'td.dt-control', function (e) {
        let tr = e.target.closest('tr');
        let row = table.row(tr);

        row.child.isShown() ? row.child.hide() : row.child(format(row.data())).show();
    });

    table.on('draw', () => {
        detailRows.forEach((no, i) => {
            let el = document.querySelector('#' + no + ' td.dt-control');
            if (el) { el.dispatchEvent(new Event('click', { bubbles: true })) }
        });
    });

    function format(d) {
        let data = d.properties;

        return (
            `<span class='ms-2 text-danger'><b>OLD : </b> ${JSON.stringify(data.old) ?? 'No Data'}</span>
            <br>
            <span class='ms-2 text-primary'><b>NEW : </b> ${JSON.stringify(data.attributes) ?? 'No Data'}</span>`
        );
    }
    // END SHOW DETAIL
}

export function filterTable () {
    table.draw()
}

export function alertDelete (id, endpoint) {
    return new Promise((resolve) => {
        Swal.fire({
            title: 'Hapus Data',
            text: "Yakin akan hapus data sekarang?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#1761FD',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: endpoint,
                    type: 'DELETE',
                    data: {
                        id: id,
                        _token: token
                    },
                    success: function(result) {
                        resolve(result);
                    }
                })
            }
        })
    });
}

export function actionPost (type, form_data, endpoint, submit = null, cancel = null, modal = null, table_update = null) {
    submit != null ? event_submit(submit, cancel) : '';

    $.ajaxSetup({ headers: {'X-CSRF-TOKEN': token }});
    $.ajax({
        type: type,
        url: base_url + endpoint,
        cache: false,
        data: form_data,
        success: function(response) {
            if (response.code == 200) {
                $.toast({heading: 'Berhasil', text: response.message, icon: 'success', position: 'top-right'});
                cancel != null ? error_submit(submit, cancel) : '';

                modal != null ? close_modal(modal) : '';
                table_update != null ? table.draw() : '';

            }
        },
        error: function(response) {
            let errors = response.responseJSON.message
            $.toast({heading: 'Gagal', text: errors, icon: 'error', position: 'top-right'})
            cancel != null ? error_submit(submit, cancel) : '';
        }
    });
};

export function event_submit (submit, cancel) {
    submit.html("<div class='spinner-border spinner-border-sm me-2'></div> Tunggu Sebentar").attr('disabled', 'disabled');
    cancel.prop('disabled', 'disabled');
}

function error_submit (submit, cancel) {
    submit.html("Simpan Data").prop('disabled', false);
    cancel.prop('disabled', false);
}

function close_modal (modal_id) {
    $(modal_id).modal('hide');
}

$('#date-range').daterangepicker({
    opens: 'center',
    ranges: {
        'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
        '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
        'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
        'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
}, function(start, end, label) {
    $('#start_date').val(start.format('Y-MM-D'));
    $('#end_date').val(end.format('Y-MM-D'));
    $('#reportrange').val(start.format('D MMM YYYY') +' - '+ end.format('D MMM YYYY'))
});














// BELUM DIGUNAKAN
jQuery.validator.setDefaults({
    errorElement: 'span',
    errorPlacement: function(error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group div').append(error);
    },
    highlight: function(element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});

function action_get(endpoint){
    $.ajax({
        type: "GET",
        url: endpoint,
        cache: false,
        success: function(response) {
            return response
        },
        error: function() {
            swal('', "Terjadi kesalahan pada sistem / session telah berakhir", 'error');
        }
    });
}

function formatRibuan(val){
    var	harga = val.toString().split('').reverse().join(''),
        hargaRibuan = harga.match(/\d{1,3}/g);
        hargaRibuan	= hargaRibuan.join('.').split('').reverse().join('');
    return hargaRibuan;
}

var my_date_format = function(input,type){
    var d = new Date(Date.parse(input.replace(/-/g, "/")));
    var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var date = d.getDate() + " " + month[d.getMonth()] + " " + d.getFullYear();
    if(type==="date"){
        return (date);
    }else{
        var hour = d.getHours();
        if(hour < 10){ hour = "0"+hour; }
        var minute = d.getMinutes();
        if(minute < 10){ minute = "0"+minute; }
        var time = hour+":"+minute;
        return (date + " " + time);
    }
};

function convertToRupiah(angka) {
    var rupiah = '';
    if (angka != null && angka != "") {
        var angkarev = angka.toString().split('').reverse().join('');
        for (var i = 0; i < angkarev.length; i++)
            if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
        angka = rupiah.split('', rupiah.length - 1).reverse().join('');
    }

    return angka
}

function convertToAngka(rupiah) {
    return parseInt(rupiah.replace(/,.*|[^0-9]/g, ''), 10);
}

function formatDateByMoment(date){
    moment.locale('id');
    return moment(date).format('dddd, LL');
}
function formatDateByMoment1(date){
    moment.locale('id');
    return moment(date).format('LL, H:m');
}


