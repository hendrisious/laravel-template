@section('title', 'Edit Profile')

@push('custom-style')
    @vite(['resources/assets/css/custom/profile.css'])
@endpush

@push('custom-script')
    @vite(['resources/assets/js/apps/profile/profile.js'])
@endpush

<x-app-layout>
    <div class="page-content">
        <div class="container-fluid">
            <div class="row mt-2 d-flex justify-content-center">
                <div class="col-lg-8">
                    @include('profile.partials.upload-profile')
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12">
                    <form autocomplete="off" id="form-profile" enctype="multipart/form-data">
                        @csrf
                        <div class="card">
                            <div class="card-header">
                                <h4 class="text-lg">
                                    {{ __('Profile Information') }}
                                </h4>

                                <p class="mt-1 text-sm">
                                    {{ __("Update your account's profile information and email address.") }}
                                </p>
                            </div>
                            <div class="card-body">
                                <div class="max-w-xl">
                                    @include('profile.partials.update-profile-information-form')
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="text-lg font-medium text-gray-900">
                                {{ __('Update Password') }}
                            </h4>

                            <p class="mt-1 text-sm text-gray-600">
                                {{ __('Ensure your account is using a long, random password to stay secure.') }}
                            </p>
                        </div>
                        <div class="card-body">
                            <div class="max-w-xl">
                                @include('profile.partials.update-password-form')
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="text-lg font-medium text-gray-900">
                                {{ __('Delete Account') }}
                            </h4>
                        </div>
                        <div class="card-body">
                            <p class="mt-1 text-sm text-gray-600">
                                {{ __('Once your account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.') }}
                            </p>
                            <div class="max-w-xl">
                                @include('profile.partials.delete-user-form')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

