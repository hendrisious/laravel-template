<section>
    <div class="modal" id="modal-upload">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title">Upload Foto Profil</h4>
                </div>
                <div class="modal-body">
                    <div id="resizer"></div>
                    <div class="d-flex justify-content-center pt-0 mt-0">
                        <button class="btn rotate float-lef" data-deg="90" ><i class="fas fa-undo"></i></button>
                        <button class="btn rotate float-right" data-deg="-90" ><i class="fas fa-redo"></i></button>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="cancel-upload">Batalkan</button>
                    <button class="btn btn-primary" id="upload">Upload Sekarang</button>
                </div>
            </div>
        </div>
    </div>
</section>
