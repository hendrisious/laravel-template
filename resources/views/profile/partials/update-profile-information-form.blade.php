<section>
    <form id="send-verification" method="post" action="{{ route('verification.send') }}">
        @csrf
    </form>

    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="outer">
                <div class="img-outer profile-img">
                    <img src="{{ $photo }}" id="profile-pic" alt="">
                </div>
                <div class="inner">
                    <input type="file" class="file-upload" id="file-upload" name="profile_picture" accept="image/*">
                    <label>
                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 20 17">
                            <path
                                d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z">
                            </path>
                        </svg>
                    </label>
                </div>
            </div>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-12">
            <form class="mt-6 space-y-6 form-profile" enctype="multipart/form-data">
                @csrf
                @method('patch')
                <div>
                    <x-input-label for="name" :value="__('Nama')" />
                    <x-text-input id="name" name="name" type="text" class="mt-1 form-control shadow-none"
                        :value="old('name', $user->name)" required autofocus autocomplete="name" />
                    <x-input-error class="mt-2" :messages="$errors->get('name')" />
                </div>

                <div class="mt-2">
                    <x-input-label for="email" :value="__('Email')" />
                    <x-text-input id="email" name="email" type="email" class="mt-1 form-control shadow-none"
                        :value="old('email', $user->email)" required autocomplete="username" />
                    <x-input-error class="mt-2" :messages="$errors->get('email')" />

                    @if ($user instanceof \Illuminate\Contracts\Auth\MustVerifyEmail && !$user->hasVerifiedEmail())
                        <div>
                            <p class="text-sm mt-2 text-gray-800">
                                {{ __('Your email address is unverified.') }}

                                <button form="send-verification"
                                    class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                    {{ __('Click here to re-send the verification email.') }}
                                </button>
                            </p>

                            @if (session('status') === 'verification-link-sent')
                                <p class="mt-2 font-medium text-sm text-green-600">
                                    {{ __('A new verification link has been sent to your email address.') }}
                                </p>
                            @endif
                        </div>
                    @endif
                </div>
                <div class="mt-2">
                    <x-input-label for="handphone" :value="__('No Handphone')" />
                    <x-text-input id="handphone" name="handphone" type="text" class="mt-1 form-control shadow-none"
                        :value="old('handphone', $user->handphone)" autocomplete="handphone" />
                    <x-input-error class="mt-2" :messages="$errors->get('handphone')" />
                </div>
                <div class="d-flex justify-content-end mt-3">
                    <x-primary-button type="submit" class="btn btn-primary"
                        id="submit-profile">{{ __('Simpan Profil') }}</x-primary-button>

                    @if (session('status') === 'profile-updated')
                        <p x-data="{ show: true }" x-show="show" x-transition x-init="setTimeout(() => show = false, 2000)"
                            class="text-sm text-gray-600">{{ __('Saved.') }}</p>
                    @endif
                </div>
            </form>
        </div>
    </div>

</section>

@include('profile.partials.upload-profile')
