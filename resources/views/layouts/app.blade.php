<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('includes.app.meta')
        <title>@yield('title')</title>
        <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}">
        @vite(['resources/css/app.css'])
        @stack('custom-style')
    </head>
    <body class="vertical-layout vertical-menu-modern navbar-floating footer-static" data-open="click" data-menu="vertical-menu-modern" data-col="">

        @include('includes.app.menu')
        <div class="page-wrapper">
            @include('includes.app.header')
            {{ $slot }}
        </div>
        @include('includes.app.footer')
        @stack('custom-script')

    </body>
</html>
