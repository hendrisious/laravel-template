<x-guest-layout>
    <x-auth-session-status class="mb-4 text-success" :status="session('status')" />

    <form class="form-horizontal auth-form form-signin">
        @csrf
        <div class="form-group mb-2">
            <label for="email" class="form-label mb-1">Email</label>
            <input id="email" class="form-control shadow-none" type="email" name="email" value="{{ old('email') }}" required autofocus autocomplete="username" />
        </div>

        <div class="form-group mb-3">
            <label for="password" class="form-label mb-1">Password</label>
            <input id="password" class="form-control shadow-none" type="password" name="password" required autocomplete="current-password" />
        </div>

        <div class="d-flex justify-content-between mt-2">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="remember" name="remember">
                <label class="form-check-label" for="remember">
                    Ingatkan saya
                </label>
            </div>

            @if (Route::has('password.request'))
                <a class="text-primary" href="{{ route('password.request') }}">
                    Lupa Password ?
                </a>
            @endif
        </div>

        <div class="mt-2">
            <button id="submit-login" class="btn btn-primary w-100 waves-effect waves-light">
                Masuk <i class="fas fa-sign-in-alt ms-1"></i>
            </button>
        </div>
    </form>
</x-guest-layout>
