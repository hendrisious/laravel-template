<x-guest-layout>
    <div class="mb-3 text-sm text-muted">
        {{ __('Lupa kata sandi Anda? Tidak masalah. Beri tahu kami alamat email Anda dan kami akan mengirimi Anda link untuk atur ulang kata sandi melalui email.') }}
    </div>

    <!-- Session Status -->
    <x-auth-session-status class="mb-3 text-success" :status="session('status')" />

    <form method="POST" action="{{ route('password.email') }}">
        @csrf

        <!-- Email Address -->
        <div>
            <x-input-label for="email" :value="__('Email')" />
            <x-text-input id="email" class="mt-1 w-100 form-control shadow-none" type="email" name="email" :value="old('email')" required autofocus />
            <x-input-error :messages="$errors->get('email')" class="mt-2 text-danger" />
        </div>

        <div class="mt-3">
            <button class="btn btn-success w-100">
                {{ __('Atur Ulang Kata Sandi') }}
            </button>
        </div>
    </form>
</x-guest-layout>
