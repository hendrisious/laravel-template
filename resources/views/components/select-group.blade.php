@props(['label', 'name', 'options' => null, 'id' => null, 'all' => false])

<label class="mb-1" for="{{ $id }}">{{ $label }}</label>
<select class="form-control w-100" name="{{ $name }}" id="{{ $id }}">
    @if ($all == true)
    <option value="">Semua</option>
    @endif
    @if ($options)
        @foreach ($options as $dt)
            <option value="{{ $dt->id }}">{{ strtoupper($dt->name) }}</option>
        @endforeach
    @endif
</select>
