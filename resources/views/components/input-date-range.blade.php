@props(['label'])

<label class="mb-1">{{ $label }}</label>
<div class="input-group w-100">
    <input type="text" class="form-control" id="reportrange" readonly>
    <span class="input-group-text bg-white" style="cursor: pointer" id="date-range"><i class="ti ti-calendar font-16 text-primary"></i></span>
</div>
<input type="hidden" name="start_date" id="start_date">
<input type="hidden" name="end_date" id="end_date">
