@props(['label', 'name', 'id' => null, 'placeholder' => null, 'type' => 'text'])

<label for="{{ $id }}" class="mb-1">{{ $label }}</label>
<input type="{{ $type }}" class="form-control" name="{{ $name }}" id="{{ $id }}" placeholder="{{ $placeholder }}">
