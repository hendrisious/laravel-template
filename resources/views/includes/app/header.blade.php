<div class="topbar">
    <nav class="navbar-custom">
        <ul class="list-unstyled topbar-nav float-end mb-0">
            <li class="dropdown">
                <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    @if (auth()->user()->image == null)
                        <img src="{{ asset('assets/images/no_image_user.png') }}" alt="profile-user" class="rounded-circle thumb-xs" />
                    @else
                        <img src="{{ route('profile.photo', ['file_name' => auth()->user()->image]) }}" alt="profile-user" class="rounded-circle thumb-xs" />
                    @endif

                    <span class="ms-1 nav-user-name hidden-sm"><b>{{ strtoupper(auth()->user()->name) }}</b></span>
                </a>
                <div class="dropdown-menu dropdown-menu-end">
                    <a class="dropdown-item" href="{{ url('profile') }}"><i data-feather="user" class="align-self-center icon-xs icon-dual me-1"></i> Pengaturan Akun</a>
                    <div class="dropdown-divider mb-0"></div>
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <a class="dropdown-item" href="route('logout')" onclick="event.preventDefault(); this.closest('form').submit();">
                            <i data-feather="power" class="align-self-center icon-xs icon-dual me-1"></i> Logout
                        </a>
                    </form>
                </div>
            </li>
        </ul>

        <ul class="list-unstyled topbar-nav mb-0">
            <li>
                <button class="nav-link button-menu-mobile">
                    <i data-feather="menu" class="align-self-center topbar-icon"></i>
                </button>
            </li>
        </ul>
    </nav>
</div>
