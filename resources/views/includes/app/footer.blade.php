<footer class="footer text-end">
    PIKNIK AJA &copy; <script>
        document.write(new Date().getFullYear())
    </script>
</footer>

<!-- jQuery  -->
<script src="{{ asset('assets/js/jquery.min.js')}}"></script>
<script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/js/metismenu.min.js') }}"></script>
<script src="{{ asset('assets/js/waves.js') }}"></script>
<script src="{{ asset('assets/js/moment.js') }}"></script>

@vite(['resources/js/app.js'])
