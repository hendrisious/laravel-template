<div class="left-sidenav">

    <div class="brand">
        <a href="{{ route('dashboard') }}" class="logo">
            <span>
                <img src="{{ asset('assets/images/logo.png') }}" alt="logo" class="logo-sm" style="height:55px">
            </span>
        </a>
    </div>

    <div class="menu-content h-100" data-simplebar>
        <ul class="metismenu left-sidenav-menu">
            @foreach (session('menus') as $label => $groups)
                <li class='menu-label my-0'>{{ $label }}</li>
                @foreach ($groups as $group => $names)
                    @php
                        $active = request()->is($names[0]->link . '*') ? "text-bold bg-soft-primary p-1 ps-2 w-100 rounded" : '';
                    @endphp
                    @if ($names[0]->name == '-')
                    <li><a href="{{ url($names[0]->link) }}"><i class="las {{ $names[0]->icon }} align-self-center menu-icon"></i><span class="{{ $active }}">{{ $group }}</span></a></li>
                    @else
                    <li>
                        <a href="javascript: void(0);">
                            <i class="las {{ $names[0]->icon }} align-self-center menu-icon"></i> <span>{{ $names[0]->group }}</span>
                            <span class='menu-arrow'><i class='mdi mdi-chevron-right'></i></span>
                        </a>
                        <ul class='nav-second-level' aria-expanded='false'>
                            @foreach ($names as $name)
                            @php
                                $active_link = request()->is($name->link . '*') ? ' bg-soft-primary p-1 ps-2 w-100 rounded' : '';
                            @endphp
                            <li class='nav-item'>
                                <a href="{{ url($name->link) }}" class="nav-link {{ $active_link }}"><i class="ti-control-record"></i>{{ $name->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                    @endif
                @endforeach
            @endforeach
        </ul>
    </div>
</div>
