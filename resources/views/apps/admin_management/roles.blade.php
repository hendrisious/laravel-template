@section('title', 'Role Manajemen')
@push('custom-script')
    @vite('resources/assets/js/apps/admin_management/roles.js')
@endpush

<x-app-layout>
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <h4 class="page-title">DATA USER ROLES</h4>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 text-end">
                                @can('delete', $url)
                                    <button class="btn btn-outline-danger" title="Sampah Roles" onclick="openModalLevel('add')"><i class="las la-recycle"></i></button>
                                @endcan
                                @can('create', $url)
                                    <button class="btn btn-primary" onclick="openModalLevel('add')"><i class="la la-plus me-1"></i> Tambah Role</button>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered w-100 mb-0" id="table-level">
                                    <thead>
                                        <tr class="text-center">
                                            <th style="width:50px">NO</th>
                                            <th>NAMA</th>
                                            <th>KEAMANAN</th>
                                            <th>TANGGAL BUAT</th>
                                            <th style="width:180px">ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_level" tabindex="-1" role="dialog" aria-labelledby="modal_level" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title m-0" id="modal_level_header"></h6>
                </div>
                <form class="form-level" method="POST">
                    @csrf
                    <div class="modal-body ps-4 pe-4">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-3 row">
                                    <label for="job_title" class="form-label align-self-center mb-lg-0 text-start">Nama</label>
                                    <input class="form-control" type="text" value="" id="user_level_name" name="name" placeholder="Contoh: Administrator">
                                    <input type="hidden" id="user_level_id" name="role_id">
                                </div>
                                <div class="form-group mb-3 row">
                                    <label for="tipe" class="form-label align-self-center mb-lg-0 text-start">Tipe Keamanan</label>
                                    <select name="guard" id="tipe" class="form-control">
                                        <option value="admin">Admin</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-soft-secondary btn-md" id="close-modal" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-md" id="submit-roles">Simpan Data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
