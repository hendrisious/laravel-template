@section('title', 'Log Activity')
@push('custom-script')
    @vite('resources/assets/js/apps/admin_management/activity.js')
@endpush

<x-app-layout>
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row">
                            <div class="col">
                                <h4 class="page-title">LOG AKTIVITAS</h4>
                            </div>
                            <div class="col-auto align-self-center">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" id="search-form" class="form-inline" role="form">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-2">
                                        <x-input-group-text
                                            label='Nama Log' name='log-name' id='log-name' placeholder='Isikan Nama Log' />
                                    </div>
                                    <div class="col-lg-2">
                                        <x-input-group-text
                                            label='Nama Staff' name='staff-name' id='staff-name' placeholder='Isikan Nama Staff' />
                                    </div>
                                    <div class="col-lg-2">
                                        <x-input-group-text
                                            label='Email Staff' name='email' id='email'
                                            placeholder='Isikan Email Staff' />
                                    </div>
                                    <div class="col-lg-2">
                                        <x-select-group label="Pilih Role" name="role" :options="get_roles()" id="role" :all="true" />
                                    </div>
                                    <div class="col-lg-1">
                                        @php
                                            $activities = collect([
                                                (object)['id' => 'created', 'name' => 'created'],
                                                (object)['id' => 'updated', 'name' => 'updated'],
                                                (object)['id' => 'deleted', 'name' => 'deleted'],
                                            ]);
                                        @endphp
                                        <x-select-group label="Aktivitas" name="event" :options="$activities" id="event" :all="true" />
                                    </div>
                                    <div class="col-lg-2">
                                        <x-input-date-range label="Range Tanggal"></x-input-date-range>
                                    </div>
                                    <div class="col-lg-1 row">
                                        <label class="mb-4"></label>
                                        <button type="submit" class="btn btn-soft-primary w-100">
                                            <i class="las la-filter me-2"></i>FILTER
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered w-100 mb-0" id="activity-table">
                                    <thead>
                                        <tr class="text-center">
                                            <th>NO</th>
                                            <th>NAMA LOG</th>
                                            <th>AKTIVITAS</th>
                                            <th>NAMA STAFF</th>
                                            <th>EMAIL STAFF</th>
                                            <th>ROLE</th>
                                            <th>TANGGAL</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="modal_level" tabindex="-1" role="dialog" aria-labelledby="modal_level" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title m-0" id="modal_level_header"></h6>
                </div>
                <form class="form-level" method="POST">
                    @csrf
                    <div class="modal-body ps-4 pe-4">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group mb-3 row">
                                    <label for="job_title" class="form-label align-self-center mb-lg-0 text-start">Nama</label>
                                    <input class="form-control" type="text" value="" id="user_level_name" name="name" placeholder="Contoh: Administrator">
                                    <input type="hidden" id="user_level_id" name="role_id">
                                </div>
                                <div class="form-group mb-3 row">
                                    <label for="tipe" class="form-label align-self-center mb-lg-0 text-start">Tipe Keamanan</label>
                                    <select name="guard" id="tipe" class="form-control">
                                        <option value="admin">Admin</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-soft-secondary btn-md" id="close-modal" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-md" id="submit-roles">Simpan Data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
