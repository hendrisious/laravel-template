@section('title', 'Pengaturan Akses')

@push('custom-script')
    @vite('resources/assets/js/apps/admin_management/permission.js')
@endpush

<x-app-layout>
    <div class="page-content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <h4 class="page-title">ROLE AKSES {{ strtoupper($role->name) }}</h4>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 text-end">
                                <a href="javascript:history.go(-1)" id="cancel-permission" class="btn btm-sm btn-outline-secondary btn-md"><i class="las la-long-arrow-alt-left me-1"></i> KEMBALI</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-lg-12">
                    <form autocomplete="off" class="form-role-setting">
                        @csrf
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered mb-0" id="table-level">
                                        <tr class="text-center">
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Group</th>
                                            <th>Menu Label</th>
                                            <th class="text-center" style="width: 100px;">Create</th>
                                            <th class="text-center" style="width: 100px;">Read</th>
                                            <th class="text-center" style="width: 100px;">Update</th>
                                            <th class="text-center" style="width: 100px;">Delete</th>
                                        </tr>
                                            @foreach ($menus as $key => $menu)
                                            <tr>
                                                <td class="text-center">
                                                    <input class="form-check-input" type="checkbox" name="menu_id[]" id="menu_id_{{ $menu->id }}" value="{{ $menu->id }}" onclick="checkedGrup(this, {{ $menu->id }})">
                                                </td>
                                                <td><label for="menu_id_{{ $menu->id }}">{{ $menu->name == '-' ? $menu->group : $menu->name }}</label></td>
                                                <td><label for="menu_id_{{ $menu->id }}">{{ $menu->group }}</label></td>
                                                <td><label for="menu_id_{{ $menu->id }}">{{ $menu->label }}</label></td>
                                                <td class="text-center">
                                                    <div class='form-check form-switch form-switch-primary d-flex justify-content-center'>
                                                        <input class='form-check-input' type="checkbox" name="create[{{ $menu->id }}]" id="create_{{ $menu->id }}" onclick="checkedCrud(this, {{ $menu->id }})">
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <div class='form-check form-switch form-switch-primary d-flex justify-content-center'>
                                                        <input class='form-check-input' type="checkbox" name="read[{{ $menu->id }}]" id="read_{{ $menu->id }}" onclick="checkedCrud(this, {{ $menu->id }})">
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <div class='form-check form-switch form-switch-primary d-flex justify-content-center'>
                                                        <input class='form-check-input' type="checkbox" name="update[{{ $menu->id }}]" id="update_{{ $menu->id }}" onclick="checkedCrud(this, {{ $menu->id }})">
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <div class='form-check form-switch form-switch-primary d-flex justify-content-center'>
                                                        <input class='form-check-input' type="checkbox" name="delete[{{ $menu->id }}]" id="delete_{{ $menu->id }}" onclick="checkedCrud(this, {{ $menu->id }})">
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-end">
                                <input type="hidden" name="role_id" id="role_id" value="{{ $role->id }}">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</x-app-layout>
