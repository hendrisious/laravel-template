@section('title', 'Admin Manajemen')

@push('custom-script')
    @vite('resources/assets/js/apps/admin_management/admin.js')
@endpush

<x-app-layout>
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row">
                            <div class="col">
                                <h4 class="page-title">DATA STAFF</h4>
                            </div>
                            <div class="col-auto align-self-center">
                                @can('create', $url)
                                    <a class="btn btn-primary" data-toggle="tooltip" data-placement="left"
                                        title="Tambah Admin" id="add" href="#"> <i class="la la-plus"></i>
                                        TAMBAH STAFF</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" id="search-form" class="form-inline" role="form">
                                @csrf
                                <div class="row">
                                    <div class="col-md-2">
                                        <x-input-group-text
                                            label='Nama Staff' name='name' id='name' placeholder='Isikan Nama Staff' />
                                    </div>
                                    <div class="col-md-2">
                                        <x-input-group-text
                                            label='Email' name='email' id='email' placeholder='Isikan Email' />
                                    </div>
                                    <div class="col-md-2">
                                        <x-input-group-text
                                            label='Handphone' name='handphone' id='handphone' placeholder='Isikan No Hp.' />
                                    </div>
                                    <div class="col-md-2">
                                        <x-select-group label="Pilih Role" name="role" :options="get_roles()" id="role" :all="true" />
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mb-1">Status</label>
                                        <select class="form-control w-100" name="status" id="status">
                                            <option value="">Semua</option>
                                            <option value="1">Aktif</option>
                                            <option value="0">Tidak Aktif</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2 row">
                                        <label class="mb-4"></label>
                                        <button type="submit" class="btn btn-soft-primary w-100">
                                            <i class="las la-filter me-2"></i>FILTER
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered w-100 mb-0" id="admin-table">
                                    <thead>
                                        <tr class="text-center">
                                            <th>NO</th>
                                            <th>NAMA</th>
                                            <th>EMAIL</th>
                                            <th>HANDPHONE</th>
                                            <th>ROLE</th>
                                            <th>VERIFIKASI</th>
                                            <th>STATUS</th>
                                            <th>TGL. DIBUAT</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-admin-modal" tabindex="-1" role="dialog" aria-labelledby="add-admin-modal-label"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title" id="add-admin-modal-label">Tambah Admin Baru</h5>
                </div>
                <form id="form-register" method="POST">
                    @csrf
                    <div class="modal-body ps-4 pe-4">
                        <div>
                            <label for="name-input">Nama Admin</label>
                            <input id="name-input" class="block form-control" type="text" name="name">
                        </div>

                        <div class="mt-2">
                            <label for="email">Email</label>
                            <input id="email-input" class="block form-control" type="email" name="email">
                        </div>

                        <div class="mt-2">
                            <label for="password">Password</label>
                            <input id="password-input" class="block form-control" type="password" name="password">
                        </div>

                        <div class="mt-2">
                            <label for="password_confirmation">Konfirmasi Password</label>
                            <input id="password_confirmation" class="block form-control" type="password"
                                name="password_confirmation">
                        </div>

                        <div class="mt-2">
                            <label for="role">Role</label>
                            <select name="role" id="role-input" class="form-control">
                                <option value="">Pilih Role</option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer pe-4">
                        <button type="button" class="btn btn-secondary close" data-bs-dismiss="modal"
                            id="close-modal">Batalkan</button>
                        <button class="btn btn-primary" id="submit-register">Tambahkan Admin</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-admin-modal" tabindex="-1" role="dialog"
        aria-labelledby="edit-admin-modal-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <h5 class="modal-title" id="edit-admin-modal-label">Edit Admin</h5>
                </div>
                <div class="modal-body ps-4 pe-4">
                    <div class="row">
                        <div class="col-lg-2 col-md-3 col-sm-12">
                            <label for="name-text">Nama</label>
                        </div>
                        <div class="col-lg-10 col-md-9 col-sm-12">
                            <h6 class="mt-0" id="name-text"></h6>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-2 col-md-3 col-sm-12">
                            <label for="email-text">Email</label>
                        </div>
                        <div class="col-lg-10 col-md-9 col-sm-12">
                            <h6 class="mt-0" id="email-text"></h6>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-2 col-md-3 col-sm-12">
                            <label for="role">Role</label>
                        </div>
                        <div class="col-lg-10 col-md-9 col-sm-12">
                            <select name="role" id="role-input-edit" class="form-control">
                                @foreach ($roles as $role)
                                    <option value="{{ $role->id }}">{{ strtoupper($role->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer pe-4">
                    <button type="button" class="btn btn-secondary close" data-bs-dismiss="modal">Batalkan</button>
                    <button type="button" class="btn btn-success" id="submit-edit-admin">Simpan Data</button>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
