<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;

use App\Models\Menu;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        $this->registerPolicies();

        Gate::define('create', function ($user, $url) {

            $access = role_access($user->role_id, $url);
            return $access = $access->create ?? false;
        });

        Gate::define('read', function ($user, $url) {

            $access = role_access($user->role_id, $url);
            return $access = $access->read ?? false;
        });

        Gate::define('update', function ($user, $url) {

            $access = role_access($user->role_id, $url);
            return $access = $access->update ?? false;
        });

        Gate::define('delete', function ($user, $url) {

            $access = role_access($user->role_id, $url);
            return $access = $access->delete ?? false;
        });
    }
}
