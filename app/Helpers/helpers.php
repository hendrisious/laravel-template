<?php

use App\Models\Menu;
use App\Models\Role;
use Illuminate\Support\Facades\Request;
use Ramsey\Uuid\Uuid as Generator;


if (!function_exists('role_access')) {
    function role_access($role_id, $url)
    {
        return Menu::roles($role_id)->where('permissions.role_id', $role_id)->where('menus.link', $url)->first();
    }
}

if (!function_exists('get_roles')) {
    function get_roles()
    {
        return Role::all();
    }
}

if (!function_exists('success_response')) {
    function success_response($code, $message)
    {
        $response = [
            'code' => $code,
            'message' => $message
        ];

        return $response;
    }
}

if (!function_exists('custom_date')) {
    function custom_date($date)
    {
        return date('d-m-Y (H:i)', strtotime($date));
    }
}

function make_uuid()
{
    return Generator::uuid4()->toString();
}
