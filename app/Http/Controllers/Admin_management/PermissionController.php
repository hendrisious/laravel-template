<?php

namespace App\Http\Controllers\Admin_management;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\PermissionRequest;
use App\Models\Menu;
use App\Models\Role;
use App\Repositories\Permission\PermissionRepository;

class PermissionController extends Controller
{
    private $permissionRepository;
    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->parent_url = 'roles';
        $this->permissionRepository = $permissionRepository;
    }

    public function setting($role_id)
    {
        $this->permission('read');
        return view('apps.admin_management.permission', [
            'url' => $this->parent_url,
            'menus' => Menu::orderBy('sort')->get(),
            'role' => Role::find($role_id)
        ]);
    }

    public function get($role_id)
    {
        $this->permission('read');
        return $this->permissionRepository->getPermission($role_id);
    }

    public function store(PermissionRequest $request)
    {
        $this->permission('create');
        return $this->permissionRepository->storePermission($request);
    }
}
