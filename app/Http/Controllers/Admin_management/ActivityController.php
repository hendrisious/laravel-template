<?php

namespace App\Http\Controllers\Admin_management;

use App\Http\Controllers\Controller;
use App\Repositories\ActivityLog\ActivityLogRepository;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    private $activityLogRepository;
    public function __construct(ActivityLogRepository $activityLogRepository)
    {
        $this->parent_url = 'activity';
        $this->activityLogRepository = $activityLogRepository;
    }

    public function index()
    {
        $this->permission('read');
        return view('apps.admin_management.activity', ['url' => $this->parent_url]);
    }

    public function getActivities(Request $request)
    {
        return $this->activityLogRepository->getActivities($request);
    }
}
