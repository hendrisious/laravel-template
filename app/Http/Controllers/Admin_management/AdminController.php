<?php

namespace App\Http\Controllers\Admin_management;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterAdminRequest;
use App\Models\Role;
use App\Repositories\Admin\AdminRepository;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AdminController extends Controller
{
    private $adminRepository;
    public function __construct(AdminRepository $adminRepository)
    {
        $this->parent_url = 'admin';
        $this->adminRepository = $adminRepository;
    }

    public function index(): View
    {
        $this->permission('read');
        return view('apps.admin_management.admin', [
            'url' => $this->parent_url,
            'roles' => Role::all()
        ]);
    }

    public function get(Request $request)
    {
        $this->permission('read');
        return $this->adminRepository->getAdmin($request);
    }

    public function store(RegisterAdminRequest $request)
    {
        $this->permission('create');
        return $this->adminRepository->storeAdmin($request);
    }

    public function update(Request $request)
    {
        $this->permission('update');
        return $this->adminRepository->updateAdmin($request);
    }

    public function delete(Request $request)
    {
        $this->permission('delete');
        return $this->adminRepository->deleteAdmin($request);
    }
}
