<?php

namespace App\Http\Controllers\Admin_management;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RolesRequest;
use App\Repositories\Roles\RolesRepository;
use Illuminate\Http\Request;

class RolesController extends Controller
{

    private $rolesRepository;
    public function __construct(RolesRepository $rolesRepository)
    {
        $this->parent_url = 'roles';
        $this->rolesRepository = $rolesRepository;
    }

    public function index()
    {
        $this->permission('read');
        return view('apps.admin_management.roles', ['url' => $this->parent_url]);
    }

    public function get(Request $request)
    {
        $this->permission('read');
        return $this->rolesRepository->getRoles($request);
    }

    public function store(RolesRequest $rolesRequest)
    {
        $this->permission('create');
        return $this->rolesRepository->storeRoles($rolesRequest);
    }

    public function update(RolesRequest $rolesRequest)
    {
        $this->permission('update');
        return $this->rolesRepository->storeRoles($rolesRequest);
    }

    public function destroy(Request $request)
    {
        $this->permission('delete');
        return $this->rolesRepository->destroyRoles($request);
    }
}
