<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\UploadProfileRequest;
use App\Http\Requests\ProfileUpdateRequest;
use App\Repositories\Admin\AdminRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class ProfileController extends Controller
{

    public function edit(Request $request): View
    {
        $style  = ['form' => true];
        $script = ['form' => true];
        $user   = $request->user();
        $photo  = auth()->user()->image == null ? asset('assets/images/no_image_user.png') : route('profile.photo', ['file_name' => auth()->user()->image]);

        return view('profile.edit', compact(['style', 'script', 'user', 'photo']));
    }

    public function update(ProfileUpdateRequest $request)
    {
        $request->user()->fill($request->validated());

        if ($request->user()->isDirty('email')) {
            $request->user()->email_verified_at = null;
        }

        $request->user()->save();

        return success_response(200, 'Profil berhasil diperbaharui');
    }

    public function destroy(Request $request): RedirectResponse
    {
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current_password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::to('/login');
    }

    public function uploadProfile(AdminRepository $adminRepository, UploadProfileRequest $request)
    {
        return $adminRepository->uploadProfile($request);
    }

    public function showPhotoProfile($file_name)
    {
        $image = storage_path('app/image/profile/' . $file_name);

        return response()->file($image);
    }
}
