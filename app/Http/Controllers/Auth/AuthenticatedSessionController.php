<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Traits\MenuTrait;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Illuminate\Validation\ValidationException;

class AuthenticatedSessionController extends Controller
{
    use MenuTrait;
    /**
     * Display the login view.
     */
    public function create(): View
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     */
    public function store(LoginRequest $request)
    {
        $request->authenticate();
        $request->session()->regenerate();

        $admin = Auth::guard('admin')->user();

        if ($admin->active == true) {
            $this->getMenus($admin->role_id);
            return response()->json([
                'code' => 200,
                'redirect' => '/dashboard'
            ]);
        } else {
            Auth::guard('admin')->logout();
            throw ValidationException::withMessages([
                'message' => trans('Akun Anda tidak aktif'),
            ]);
        }
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('admin')->logout();

        $request->session()->forget('menus');
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/');
    }
}
