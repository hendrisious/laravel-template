<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterAdminRequest;
use App\Providers\RouteServiceProvider;
use App\Repositories\Admin\AdminRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class RegisteredUserController extends Controller
{

    public function create(): View
    {
        return view('auth.register');
    }

    public function store(AdminRepository $adminRepository, RegisterAdminRequest $request): RedirectResponse
    {
        $admin = $adminRepository->storeAdmin($request);
        Auth::guard('admin')->login($admin);

        return redirect(RouteServiceProvider::HOME);
    }
}
