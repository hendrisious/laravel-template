<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Gate;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    protected $parent_url;
    protected $role_id;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->role_id = auth()->user()->role_id ?? '';

            return $next($request);
        });
    }

    protected function permission($permission)
    {
        if (!Gate::allows($permission, $this->parent_url)) {
            return abort(403, 'Maaf, anda tidak memiliki akses');
        }
    }
}
