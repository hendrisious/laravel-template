<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct(Request $request)
    {
        // ROLES
        $this->parent_url = 'dashboard';
    }
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $this->permission('read');
        return view('dashboard');
    }
}
