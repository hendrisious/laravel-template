<?php

namespace App\Traits;

use App\Models\Permission;

trait MenuTrait
{
    public function getMenus($role_id)
    {
        $menus = Permission::select('role_id', 'menus.label', 'menus.group', 'menus.name', 'menus.link', 'menus.icon')
            ->leftJoin('menus', 'menu_id', 'menus.id')->where('role_id', $role_id)->orderBy('sort', 'ASC')->get();

        $menus      = collect($menus)->groupBy('label');
        $group_menu = array();

        foreach ($menus as $key => $item) {
            $group_menu[$key] = $item;
            foreach ($item as $it) {
                $group_menu[$key] = collect($item)->groupBy('group');
            }
        }

        session()->put('menus', $group_menu);
    }
}
