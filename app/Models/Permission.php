<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Traits\Uuid;

class Permission extends Model
{
    use HasFactory, Uuid, LogsActivity;

    public $incrementing = false;

    protected $guarded = [];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['role_id', 'menu_id', 'create', 'read', 'update', 'delete'])
            ->setDescriptionForEvent(fn (string $eventName) => "This model has been {$eventName}")
            ->useLogName('Permission');
    }

    public function scopeRole($query, $role_id)
    {
        return $query->where('role_id', $role_id)->get();
    }

    public function scopeRoleMenu($query, $array)
    {
        foreach ($array as $field => $value) {
            $query->where($field, $value);
        }
        return $query;
    }
}
