<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuid;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Role extends Model
{
    use HasFactory, SoftDeletes, Uuid, LogsActivity;

    protected $guarded = [];
    public $incrementing = false;

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logOnly(['name', 'guard_name'])
            ->setDescriptionForEvent(fn (string $eventName) => "This model has been {$eventName}")
            ->useLogName('Roles');
    }

    public function permissions()
    {
        return $this->hasMany(Permission::class, 'role_id');
    }

    protected static function booted()
    {
        static::deleting(function ($role) {
            $role->permissions()->get()->each->delete();
        });

        static::created(function ($role) {
            $role->permissions()->create(['role_id' => $role->id, 'menu_id' => 1]);
        });
    }
}
