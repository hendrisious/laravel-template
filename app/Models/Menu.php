<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function scopeRoles($query, $role_id)
    {
        return $query->select('link', 'role_id', 'create', 'read', 'update', 'delete')->from('menus')
            ->leftJoin('permissions', 'menus.id', '=', 'permissions.menu_id', 'AND', 'permissions.role_id', '=', $role_id);
    }

    public function permissions()
    {
        return $this->hasMany(Permission::class, 'menu_id', 'id');
    }
}
