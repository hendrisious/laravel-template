<?php

namespace App\Repositories\ActivityLog;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use LaravelEasyRepository\Implementations\Eloquent;
use Spatie\Activitylog\Models\Activity;
use DataTables;

class ActivityLogRepositoryImplement extends Eloquent implements ActivityLogRepository
{

    protected $model;

    public function __construct(Activity $model)
    {
        $this->model = $model;
    }

    public function getActivities($request)
    {
        if ($request->ajax()) {
            $data = $this->model->with('causer', 'causer.role')->select('activity_log.*')->orderBy('id', 'DESC');

            if (empty($request->get('start_date'))) {
                $data = $data->where(DB::raw('DATE(created_at)'), Carbon::today()->toDateString());
            }

            return Datatables::of($data)->addIndexColumn()->filter(function ($instance) use ($request) {

                if (!empty($request->get('log_name'))) {
                    $instance->where('log_name', 'like', "" . $request->get('log_name') . "%");
                }

                if (!empty($request->get('event'))) {
                    $instance->where('event', $request->get('event'));
                }

                if (!empty($request->get('staff_name'))) {
                    $instance->whereRelation('causer', 'name', 'like', "" . $request->get('staff_name') . "%");
                }

                if (!empty($request->get('email'))) {
                    $instance->whereRelation('causer', 'email', 'like', "" . $request->get('email') . "%");
                }

                if (!empty($request->get('start_date'))) {
                    $instance->whereBetween(DB::raw('DATE(created_at)'), [
                        $request->get('start_date'),
                        $request->get('end_date')
                    ]);
                }

                if (!empty($request->get('search'))) {
                    $instance->where('log_name', 'like', "" . $request->get('search') . "%")
                        ->orWhere('event', 'like', "" . $request->get('search') . "%");
                }
            })
                ->editColumn('created_at', '{{ custom_date($created_at) }}')
                ->editColumn('event', function ($row) {

                    switch ($row->event) {
                        case 'created':
                            $color = 'primary';
                            break;
                        case 'updated':
                            $color = 'success';
                            break;
                        case 'deleted':
                            $color = 'danger';
                            break;
                    }

                    return "<span class='badge badge-soft-" . $color . "'>$row->event</span>";
                })
                ->addColumn('action', function ($row) {
                    return "<span style='cursor: pointer' class='badge badge-soft-primary'><i class='las la-arrow-down font-13 me-1'></i> Lihat Detail</span>";
                })
                ->rawColumns(['action', 'no', 'event'])->make(true);
        }
    }
}
