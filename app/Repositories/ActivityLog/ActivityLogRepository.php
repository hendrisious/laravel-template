<?php

namespace App\Repositories\ActivityLog;

use LaravelEasyRepository\Repository;

interface ActivityLogRepository extends Repository
{
    public function getActivities($request);
}
