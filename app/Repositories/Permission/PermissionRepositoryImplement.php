<?php

namespace App\Repositories\Permission;

use LaravelEasyRepository\Implementations\Eloquent;
use App\Models\Permission;
use App\Models\Role;
use App\Traits\MenuTrait;

class PermissionRepositoryImplement extends Eloquent implements PermissionRepository
{

    use MenuTrait;

    protected $model;

    public function __construct(Permission $model)
    {
        $this->model = $model;
    }

    public function getPermission($role_id)
    {
        return $this->model->role($role_id);
    }

    public function storePermission($request)
    {
        $check_roles = Role::find($request->role_id);

        if ($check_roles) {
            try {
                $role_type = $request->create == 'false' && $request->read == 'false' && $request->update == 'false' && $request->delete == 'false';
                $role_menu = array('menu_id' => $request->menu_id, 'role_id' => $request->role_id);

                if ($role_type) {
                    $permission = $this->model->roleMenu($role_menu)->first();
                    $this->model->destroy($permission->id);
                    $message = 'Akses dihilangkan';
                } else {
                    $this->model->updateOrCreate(
                        ['role_id' => $request->role_id, 'menu_id' => $request->menu_id],
                        [
                            'create' => $request->create == 'true' ? 1 : 0,
                            'read'   => $request->read == 'true' ? 1 : 0,
                            'update' => $request->update == 'true' ? 1 : 0,
                            'delete' => $request->delete == 'true' ? 1 : 0
                        ]
                    );
                    $message = 'Akses berhasil diperbaharui';
                }

                session()->forget('menus');
                $this->getMenus(auth()->user()->role_id);

                return success_response(200, $message);
            } catch (\Throwable $e) {
                return success_response(500, $e->getMessage());
            }
        } else {
            return success_response(500, 'Role tidak ditemukan');
        }
    }
}
