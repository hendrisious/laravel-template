<?php

namespace App\Repositories\Permission;

use LaravelEasyRepository\Repository;

interface PermissionRepository extends Repository
{
    public function getPermission($role_id);
    public function storePermission($request);
}
