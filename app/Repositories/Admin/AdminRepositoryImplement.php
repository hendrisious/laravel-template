<?php

namespace App\Repositories\Admin;

use LaravelEasyRepository\Implementations\Eloquent;
use App\Models\Admin;
use DataTables;
use Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use App\Traits\UploadImageTrait;

use function PHPUnit\Framework\isTrue;

class AdminRepositoryImplement extends Eloquent implements AdminRepository
{
    use UploadImageTrait;

    protected $model, $access;

    public function __construct(Admin $model)
    {
        $this->model = $model;
    }

    public function getAdmin($request)
    {
        if ($request->ajax()) {
            $data = $this->model->select('admins.*')->with('role');

            return Datatables::of($data)->addIndexColumn()->filter(function ($instance) use ($request) {

                if (!empty($request->get('name'))) {
                    $instance->where('name', 'like', "" . $request->get('name') . "%");
                }

                if (!empty($request->get('email'))) {
                    $instance->where('email', 'like', "" . $request->get('email') . "%");
                }

                if (!empty($request->get('handphone'))) {
                    $instance->where('handphone', 'like', "" . $request->get('handphone') . "%");
                }

                if (!empty($request->get('role'))) {
                    $instance->where('role_id', $request->get('role'));
                }

                if ($request->get('status') == '0' || $request->get('status') == '1') {
                    $instance->where('active', $request->get('status'));
                }

                if (!empty($request->get('search'))) {

                    $instance->where('admins.name', 'like', "" . $request->get('search') . "%")
                        ->orWhere('email', 'like', "" . $request->get('search') . "%");
                }
            })
                ->editColumn('name', '{{ Str::upper($name) }}')
                ->editColumn('created_at', '{{ custom_date($created_at) }}')
                ->editColumn('role', function ($row) {
                    return strtoupper($row->role->name);
                })
                ->addColumn('verified', function ($row) {
                    return is_null($row->email_verified_at) ? '-' : '<i class="las la-check font-18 text-primary"></i>';
                })
                ->addColumn('actived', function ($row) {
                    $access    = role_access(auth()->user()->role_id, 'admin');
                    $condition = $row->active == true ? 'checked' : '';
                    $togle     = "<div class='d-flex justify-content-center'>";
                    if ($access->update == true) {
                        $togle  .= "<div class='form-check form-switch form-switch-success'>
                                        <input class='form-check-input' type='checkbox' onchange='changeStatus(`$row->id`, `$row->role_id`, this)' $condition style='cursor:pointer'>
                                    </div>";
                    } else {
                        $togle  .= $row->active === true ? '<i class="las la-toggle-on text-success font-24"></i>' : '<i class="las la-toggle-off text-secondary font-24"></i>';
                    }
                    $togle .= "</div>";

                    return $togle;
                })
                ->addColumn('action', function ($row) {
                    $access = role_access(auth()->user()->role_id, 'admin');
                    $button = '';

                    if ($access->update == true) {
                        $button .= "<button class='btn btn-soft-success btn-xs me-1'
                                        onClick='editAdmin(`$row->id`, `$row->name`, `$row->email`, `$row->role_id`, $row->active)'>
                                        <i class='las la-pen font-13'></i>
                                    </button>";
                    }

                    if ($access->delete == true) {
                        $button .= "<button class='btn btn-soft-danger btn-xs' onClick='deleteAdmin(`$row->id`)'>
                                        <i class='las la-trash-alt font-13'></i>
                                    </button>";
                    }

                    return $button;
                })
                ->rawColumns(['actived', 'action', 'no', 'verified'])->make(true);
        }
    }

    public function storeAdmin($request)
    {
        $admin = $this->model->create([
            'role_id'   => $request->role,
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => Hash::make($request->password),
        ]);

        event(new Registered($admin));

        return success_response(200, 'Admin Berhasil Ditambahkan');

        // return $admin;
    }

    public function updateAdmin($request)
    {
        $this->model->find($request->id)->update([
            'role_id' => $request->role_id,
            'active'  => $request->active
        ]);

        return success_response(200, 'Admin Berhasil Diperbaharui');
    }

    public function uploadProfile($request)
    {
        $user = Admin::findOrFail(auth()->user()->id);

        try {
            if ($request->has('profile_picture')) {

                $image    = $request->file('profile_picture');
                $filename = time() . '-' . uniqid() . '.' . $image->guessExtension();
                $request->file('profile_picture')->storeAs('image/profile', $filename);

                $user->image = $filename;
            }

            $user->save();

            return success_response(200, 'Foto Profil berhasil diperbaharui');
        } catch (Exception $e) {
            return success_response(200, $e->getMessage());
        }
    }

    public function deleteAdmin($request)
    {
        try {
            $this->model->find($request->id)->delete($request->id);
            $response = success_response(200, 'Admin berhasil dihapus');
        } catch (Exception $e) {
            $response = success_response(500, $e);
        }

        return $response;
    }
}
