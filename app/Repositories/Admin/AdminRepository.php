<?php

namespace App\Repositories\Admin;

use LaravelEasyRepository\Repository;

interface AdminRepository extends Repository
{
    public function getAdmin($request);
    public function uploadProfile($request);
    public function storeAdmin($request);
    public function updateAdmin($request);
    public function deleteAdmin($request);
}
