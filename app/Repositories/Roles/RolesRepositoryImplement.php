<?php

namespace App\Repositories\Roles;

use App\Models\Admin;
use App\Models\Permission;
use LaravelEasyRepository\Implementations\Eloquent;
use App\Models\Role;
use DataTables;
use Illuminate\Support\Facades\DB;

class RolesRepositoryImplement extends Eloquent implements RolesRepository
{

    protected $model;

    public function __construct(Role $model)
    {
        $this->model = $model;
    }

    public function getRoles($request)
    {
        if ($request->ajax()) {
            $data = $this->model->select([DB::raw('ROW_NUMBER() OVER(ORDER BY id ASC) AS no'), 'roles.*']);

            return Datatables::of($data)->addIndexColumn()->filter(function ($instance) use ($request) {
                if (!empty($request->get('search'))) {
                    $instance->where('name', 'like', "%" . $request->get('search') . "%")
                        ->orWhere('guard_name', 'like', "" . $request->get('search') . "%");
                }
            })
                ->editColumn('no', '<center>{{ $no }}.</center>')
                ->editColumn('name', '{{ Str::upper($name) }}')
                ->editColumn('guard_name', '{{ Str::upper($guard_name) }}')
                ->editColumn('created_at', '{{ custom_date($created_at) }}')
                ->addColumn('action', function ($row) {

                    $access = role_access(auth()->user()->role_id, 'roles');
                    $url    = route('permission.setting', ['role_id' => $row->id]);
                    $button = false;

                    if ($access->update == 1) {
                        $button = "<button class='btn btn-soft-success btn-xs' onclick='openModalLevel(`$row->id`, `$row->name`, `$row->guard_name`)'><i class='las la-pen font-13'></i></button>";
                    }

                    if ($access->update == 1) {
                        $button .= "<a class='btn btn-soft-info btn-xs ms-1 me-1' href='$url'><i class='las la-cog font-13'></i></a>";
                    }

                    if ($access->delete == 1) {
                        $button .= "<button class='btn btn-soft-danger btn-xs' onclick='deleteRoles(`$row->id`)'><i class='las la-trash-alt font-13'></i></button>";
                    }

                    return $button;
                })->rawColumns(['action', 'no'])->make(true);
        }
    }

    public function storeRoles($request)
    {
        $roles_data = ['name' => $request->name, 'guard_name' => $request->guard];
        try {
            if (empty($request->role_id)) {
                $this->model->create($roles_data);

                return success_response(200, 'Role berhasil ditambahkan');
            } else {
                return $this->updateRoles($request->role_id, $roles_data);
            }
        } catch (\Throwable $e) {
            return success_response(500, $e->getMessage());
        }
    }

    private function updateRoles($role_id, $roles)
    {
        $this->model->find($role_id)->update($roles);
        return success_response(200, 'Role berhasil dirubah');
    }

    public function destroyRoles($request)
    {
        try {
            $this->model->destroy($request->id);

            $response = success_response(200, 'Role berhasil dihapus');
        } catch (\Throwable $e) {
            $response = success_response(500, $e);
        }

        return $response;
    }
}
