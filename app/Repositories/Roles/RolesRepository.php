<?php

namespace App\Repositories\Roles;

use LaravelEasyRepository\Repository;

interface RolesRepository extends Repository
{
    public function getRoles($request);
    public function storeRoles($request);
    public function destroyRoles($request);
}
