<?php

use App\Http\Controllers\Admin_management\ActivityController;
use App\Http\Controllers\Admin_management\AdminController;
use App\Http\Controllers\Admin_management\PermissionController;
use App\Http\Controllers\Admin_management\RolesController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/menu', function () {

    $group_menu = getMenus(1);
    return response()->json($group_menu);
});

Route::middleware(['auth:admin', 'verified'])->group(function () {
    Route::get('/dashboard', DashboardController::class)->name('dashboard');

    Route::prefix('profile')->controller(ProfileController::class)->group(function () {
        Route::get('/', 'edit')->name('profile.edit');
        Route::get('/photo/{file_name}', 'showPhotoProfile')->name('profile.photo');
        Route::post('/', 'uploadProfile')->name('profile.upload');
        Route::patch('/', 'update')->name('profile.update');
        Route::delete('/', 'destroy')->name('profile.destroy');
    });

    Route::prefix('admin')->controller(AdminController::class)->group(function () {
        Route::get('/', 'index')->name('admin');
        Route::get('/list', 'get')->name('admin.list');
        Route::delete('/', 'delete')->name('admin.delete');
        Route::post('/', 'store')->name('admin.store');
        Route::patch('/', 'update')->name('admin.update');
    });

    Route::prefix('roles')->controller(RolesController::class)->group(function () {
        Route::get('/', 'index')->name('roles');
        Route::get('/list', 'get')->name('roles.list');
        Route::post('/', 'store')->name('roles.store');
        Route::patch('/', 'update')->name('roles.update');
        Route::delete('/', 'destroy')->name('roles.destroy');

        Route::prefix('permission')->controller(PermissionController::class)->group(function () {
            Route::get('/setting/{role_id}', 'setting')->name('permission.setting');
            Route::get('/get/{role_id}', 'get')->name('permission.get');
            Route::post('/', 'store')->name('permission.store');
        });
    });

    Route::prefix('activity')->controller(ActivityController::class)->group(function () {
        Route::get('/', 'index')->name('activity');
        Route::get('/list', 'getActivities')->name('activity.list');
    });
});

require __DIR__ . '/auth.php';
