<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Menu;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Ramsey\Uuid\Uuid as Generator;

class DefaultDataSeeder extends Seeder
{
    public function run(): void
    {
        $now     = date('Y-m-d H:i:s');
        $role_id = Generator::uuid4()->toString();
        // Add Super Admin Role
        $role = [
            'id'         => $role_id,
            'name'       => 'super admin',
            'guard_name' => 'admin',
            'created_at' => $now,
            'updated_at' => $now,
        ];

        Role::insert($role);

        // Add Super Admin Account
        $admin = [
            'id'                => Generator::uuid4()->toString(),
            'role_id'           => $role_id,
            'name'              => 'super admin',
            'email'             => 'superadmin@email.com',
            'email_verified_at' => Carbon::now(),
            'password'          => 'password',
            'active'            => 1,
            'created_at'        => $now,
            'updated_at'        => $now
        ];

        Admin::insert($admin);

        // Add Default Menu
        $menus = [
            [
                'label' => 'MENU UTAMA',
                'group' => 'DASHBOARD',
                'name'  => '-',
                'link'  => 'dashboard',
                'icon'  => 'la-home',
                'sort'  => 1
            ],
            [
                'label' => 'PENGATURAN',
                'group' => 'MANAJEMEN USER',
                'name'  => 'ADMIN STAFF',
                'link'  => 'admin',
                'icon'  => 'la-user',
                'sort'  => 60
            ],
            [
                'label' => 'PENGATURAN',
                'group' => 'MANAJEMEN USER',
                'name'  => 'ROLES',
                'link'  => 'roles',
                'icon'  => 'la-user',
                'sort'  => 61
            ],
            [
                'label' => 'PENGATURAN',
                'group' => 'MANAJEMEN USER',
                'name'  => 'LOG ACTIVITY',
                'link'  => 'activity',
                'icon'  => 'la-sd-card',
                'sort'  => 62
            ],
        ];

        Menu::insert($menus);

        // Add Default Permission
        foreach ($menus as $key => $menu) {
            $permissions[] = [
                'id'      => Generator::uuid4()->toString(),
                'role_id' => $role_id,
                'menu_id' => $key + 1,
                'create'  => 1,
                'read'    => 1,
                'update'  => 1,
                'delete'  => 1,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }

        Permission::insert($permissions);
    }
}
