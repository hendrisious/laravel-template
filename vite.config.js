import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/guest.css',
                'resources/js/guest.js',

                'resources/css/app.css',
                'resources/js/app.js',

                'resources/js/apps.js',
                'resources/css/datatables.css',

            ],
            refresh: true,
        }),
    ],
    resolve: {
        alias: {
            '$': 'jQuery'
        },

    },
});
