
# Laravel Base Template With Roles 

Base tamplate for admin panel with Authentication and Authorization using Breeze.

### Installation

To run tests, run the following command

```bash
  git clone https://github.com/hendrisiouss/laravel-template.git
```
```bash
  composer install
```
```bash
  npm install
```
```bash
  cp .env.example .env
```
```bash
  php artisan key:generate
```
```bash
  create database and set on .env
```
```bash
  php artisan migrate --seed
```
```bash
  php artisan serve
```
```bash
  npm run dev
```



### Default login

Run this project and input this email with password :

Email :
```bash
superadmin@email.com
```
Password :
```bash
password
```
    